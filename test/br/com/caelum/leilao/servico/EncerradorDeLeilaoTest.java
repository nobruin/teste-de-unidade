package br.com.caelum.leilao.servico;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.Calendar;

import org.junit.Test;
import org.mockito.InOrder;

import br.com.caelum.leilao.builder.CriadorDeLeilao;
import br.com.caelum.leilao.dominio.Leilao;
import br.com.caelum.leilao.infra.dao.LeilaoDao;
import br.com.caelum.leilao.infra.dao.RepositorioDeLeiloes;

public class EncerradorDeLeilaoTest {
    
    @Test
    public void deveEncerrarLeiloesAntigos() {
        Calendar data = Calendar.getInstance();
        data.set(1999, 1, 20);

        Leilao leilao = new CriadorDeLeilao().para("tv").naData(data).constroi();
        Leilao leilao2 = new CriadorDeLeilao().para("tv2").naData(data).constroi();

        LeilaoDao dao = mock(LeilaoDao.class);

        when(dao.correntes()).thenReturn(Arrays.asList(leilao,leilao2));
        
        Carteiro carteiro = mock(Entregador.class);
        EncerradorDeLeilao encerrador = new EncerradorDeLeilao(dao, carteiro);
        encerrador.encerra();    
        

        assertTrue(leilao.isEncerrado());
        assertTrue(leilao2.isEncerrado());        
        assertEquals(2, encerrador.getTotalEncerrados());        

        InOrder inOrder = inOrder(dao, carteiro);
        
        inOrder.verify(dao, times(1)).atualiza(leilao);
        inOrder.verify(carteiro, times(1)).envia(leilao);

        inOrder.verify(dao, times(1)).atualiza(leilao2);
        inOrder.verify(carteiro, times(1)).envia(leilao2);        
    }

    @Test
    public void naoEncerraLeiloesRecentes(){
        Calendar data = Calendar.getInstance();
        data.add(Calendar.DAY_OF_MONTH, -1);

        Leilao leilao = new CriadorDeLeilao().para("tv").naData(data).constroi();
        Leilao leilao2 = new CriadorDeLeilao().para("tv2").naData(data).constroi();

        LeilaoDao dao = mock(LeilaoDao.class);
        when(dao.correntes()).thenReturn(Arrays.asList(leilao,leilao2));

        Carteiro carteiro = new Entregador();
        EncerradorDeLeilao encerrador = new EncerradorDeLeilao(dao, carteiro);
        encerrador.encerra();    
        

        assertFalse(leilao.isEncerrado());
        assertFalse(leilao2.isEncerrado());
        assertEquals(0, encerrador.getTotalEncerrados());        
    }

    @Test
    public void naoDeveEncerrarLeiloesQueComecaramMenosDeUmaSemanaAtras() {

        Calendar ontem = Calendar.getInstance();
        ontem.add(Calendar.DAY_OF_MONTH, -1);

        Leilao leilao1 = new CriadorDeLeilao().para("TV de plasma")
            .naData(ontem).constroi();
        Leilao leilao2 = new CriadorDeLeilao().para("Geladeira")
            .naData(ontem).constroi();

        RepositorioDeLeiloes dao = mock(LeilaoDao.class);
        when(dao.correntes()).thenReturn(Arrays.asList(leilao1, leilao2));

        Carteiro carteiro = mock(Entregador.class);
        EncerradorDeLeilao encerrador = new EncerradorDeLeilao(dao, carteiro);
        
        encerrador.encerra();

        assertEquals(0, encerrador.getTotalEncerrados());
        assertFalse(leilao1.isEncerrado());
        assertFalse(leilao2.isEncerrado());

        verify(dao, never()).atualiza(leilao1);
        verify(dao, never()).atualiza(leilao2);
    }    

    @Test
    public void deveContinuarExecucaoComExcepetion() {

        Calendar date = Calendar.getInstance();
        date.set(1999, 1, 21);

        Leilao leilao1 = new CriadorDeLeilao().naData(date).para("tv plasma").constroi();

        Leilao leilao2 = new CriadorDeLeilao().naData(date).para("tv plasma").constroi();

        RepositorioDeLeiloes dao = mock(RepositorioDeLeiloes.class);
        when(dao.correntes()).thenReturn(Arrays.asList(leilao1, leilao2));

        doThrow(new RuntimeException()).when(dao).atualiza(leilao1);

        Carteiro carteiro = mock(Entregador.class);
        EncerradorDeLeilao encerrador = new EncerradorDeLeilao(dao, carteiro);
        
        encerrador.encerra();

        verify(dao).atualiza(leilao2);
        verify(carteiro).envia(leilao2);

    }

    @Test
    public void naoDeveContinuarExecucaoComExcepetion() {

        Calendar date = Calendar.getInstance();
        date.set(1999, 1, 21);

        Leilao leilao1 = new CriadorDeLeilao().naData(date).para("tv plasma").constroi();

        Leilao leilao2 = new CriadorDeLeilao().naData(date).para("tv plasma").constroi();

        RepositorioDeLeiloes dao = mock(RepositorioDeLeiloes.class);
        when(dao.correntes()).thenReturn(Arrays.asList(leilao1, leilao2));

        doThrow(new RuntimeException()).when(dao).atualiza(any(Leilao.class));
        
        Carteiro carteiro = mock(Entregador.class);
        EncerradorDeLeilao encerrador = new EncerradorDeLeilao(dao, carteiro);
        
        encerrador.encerra();

        verify(carteiro, never()).envia(any(Leilao.class));        
    }
}