package br.com.caelum.leilao.infra.dao;

import br.com.caelum.leilao.dominio.Pagamento;

/**
 * RepositorioPagamentos
 */
public interface RepositorioPagamentos {

    void salva(Pagamento pagamento);
}